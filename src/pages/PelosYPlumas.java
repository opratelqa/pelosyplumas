package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBasePelosYPlumas;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class PelosYPlumas extends TestBasePelosYPlumas {
	
	final WebDriver driver;
	public PelosYPlumas(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInPelosYPlumas(String apuntaA) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Pelos y Plumas Peru - landing");
		
		WebElement menu1 = driver.findElement(By.xpath("//a[contains(text(), 'Home')]"));
			menu1.click();
			espera(500);
				String elemento1 = driver.findElement(By.xpath("//*[@id=\"tuyos\"]/div/div/div/h2")).getText();			
					Assert.assertEquals(elemento1,"Historias");
					System.out.println(elemento1);
			espera(500);
			
		WebElement menu2 = driver.findElement(By.xpath("//a[contains(text(), 'Historias')]"));
			menu2.click();
			espera(500);
				String elemento2 = driver.findElement(By.cssSelector(".col-md-6")).getText();
					Assert.assertEquals(elemento2,"Historias");
					System.out.println(elemento2);
			espera(500);		
					
		WebElement menu3 = driver.findElement(By.xpath("//a[contains(text(), 'Blog')]"));
			menu3.click();
			espera(500);
				String elemento3 = driver.findElement(By.cssSelector(".section-heading")).getText();
				Assert.assertEquals(elemento3,"Blog");
				System.out.println(elemento3);
			espera(500);
			
		WebElement menu4 = driver.findElement(By.xpath("//a[contains(text(), 'Paséame')]"));
			menu4.click();
			espera(500);
				String elemento4 = driver.findElement(By.xpath("/html/body/section[1]/div/div/div/header/h1")).getText();
				Assert.assertEquals(elemento4,"Paséame");
				System.out.println(elemento4);
			espera(500);
			
		WebElement menu5 = driver.findElement(By.xpath("//a[contains(text(), 'Adóptame')]"));
			menu5.click();
			espera(500);
				String elemento5 = driver.findElement(By.xpath("/html/body/section[1]/div/div/div/header/h1")).getText();
				Assert.assertEquals(elemento5,"Adóptame");
				System.out.println(elemento5);
			espera(500);
			
		WebElement menu6 = driver.findElement(By.xpath("//a[contains(text(), 'Beneficios')]"));
			menu6.click();
			espera(500);
				String elemento6 = driver.findElement(By.xpath("/html/body/section/div/div/div/h1")).getText();
				Assert.assertEquals(elemento6,"Beneficios");
				System.out.println(elemento6);
			espera(500);	
		
			
		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Pelos y Plumas Peru"); 
		System.out.println();
		System.out.println("Fin de Test Patitas Pelos y Plumas Peru - Landing");
			
	}			

	
}  

